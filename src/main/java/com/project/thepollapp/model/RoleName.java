package com.project.thepollapp.model;

public enum RoleName {
	ROLE_USER,
    ROLE_ADMIN
}
